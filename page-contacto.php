<?php get_header(); ?>
<!-- Escupe la ruta hacia la carpeta raiz del templete <?php echo get_template_directory_uri(); ?> -->

<div class="container">
    <section class="section">
        <h2><span>Contacto</span></h2>
        <ul class="breadcrumb">
            <li><a href="javascript:void(0);">Inicio</a> / </li>
        </ul>
        <!--EMPIEZA EL LOOP-->
        <div class="content contact">
            <h2>Contacto</h2>
            <p>Mantente informado acerca de nuestras exposiciones y actividades de formación o envíanos un mensaje y te responderemos muy pronto.</p>
            <div class="contact-form">
                <?php wd_form_maker(10); ?>
            </div>
        </div>
        <!--TERMINA EL LOOP-->
    </section>
</div>

<?php get_footer(); ?>