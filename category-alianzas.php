<?php get_header(); ?>
<!-- Escupe la ruta hacia la carpeta raiz del templete <?php echo get_template_directory_uri(); ?> -->

<div class="container">
    <section class="section">
        <h2><span>Colaboradores</span></h2>
        <ul class="breadcrumb">
            <li><a href="javascript:void(0);">Inicio</a> / </li>
        </ul>
        <!--EMPIEZA EL LOOP-->
        <div class="content colaboradores">
            <div class="alianzas">
                <h3>Alianzas</h3>
                <?php if (dynamic_sidebar('col-alianzas-widget')) : else : endif; ?>
            </div>
            <div class="patrocinios">
                <h3>Patrocinios</h3>
                <?php if (dynamic_sidebar('col-patrocinios-widget')) : else : endif; ?>
            </div>
            <div class="colaboraciones">
                <h3>Colaboraciones</h3>
                <?php if (dynamic_sidebar('col-colaboraciones-widget')) : else : endif; ?>
            </div>
        </div>
        <!--TERMINA EL LOOP-->
    </section>
</div>

<?php get_footer(); ?>