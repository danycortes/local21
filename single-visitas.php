<?php get_header(); ?>
<!-- Escupe la ruta hacia la carpeta raiz del templete <?php echo get_template_directory_uri(); ?> -->

<div class="container">
    <section class="section">
        <h2><span>Visitas</span></h2>
        <ul class="breadcrumb">
            <li><a href="javascript:void(0);">Inicio</a> / </li>
            <li><a href="javascript:void(0);">Visitas</a></li>
        </ul>
        <!--EMPIEZA EL LOOP-->
        <div class="content">
            <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
                <?php the_post_thumbnail( 'post-thumb' ); ?>
                <div class="info">
                    <h3><?php the_title(); ?></h3>
                    <?php the_content(); ?>
                    <div class="single-gallery"></div>
                </div>
            <?php endwhile; else: ?>
                <p><?php _e('Lo sentimos, ningún post cumple con los criterios de búsqueda.'); ?></p>
            <?php endif; ?>
        </div>
        <!--TERMINA EL LOOP-->
    </section>
</div>

<?php get_footer(); ?>