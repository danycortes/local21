<?php get_header('two'); ?>
<!-- Escupe la ruta hacia la carpeta raiz del templete <?php echo get_template_directory_uri(); ?> -->

<div class="container">
    <section class="sec-exposition">
        <h2><span>Próximamente</span></h2>
        <div class="exposition-container">
            <div class="exposition-info">
                <p>Sitio web en construcción, estamos trabajando para traerte un mejor sitio. Regresa pronto</p>
                <p>Si necesitas ponerte en contacto con nosotros, no dudes en escribirnos a
                    <a href="mailto:infolocal21@gmail.com">infolocal21@gmail.com</a>
                </p>
            </div>
        </div>
    </section>
</div>
<br><br><br><br><br><br><br><br><br><br>

<?php get_footer(); ?>