<?php
/**
 * Este es el templete para mostrar el footer del sitio
 *
 */
?>

<footer class="main-footer">
    <div class="pattern"></div>
    <div class="container">
        <h2>© Local 21. Espacios Alternativos de Arte, A.C.</h2>
        <p>Homero 820, Col. Polanco</p>
        <p>C.P. 11550. México D.F.</p>
        <p>T.(+5255)75899699/7589 9719</p>
        <a href="javascript:void(0);">Aviso de privacidad</a>
    </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>