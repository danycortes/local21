<?php get_header(); ?>
<!-- Escupe la ruta hacia la carpeta raiz del templete <?php echo get_template_directory_uri(); ?> -->

<div class="container">
    <section class="sec-exposition">
        <h2><span>Actividades</span></h2>
        <div class="exposition-container">
            <?php $catquery = new WP_Query( array(
                'category_name' => 'post-home',
                'posts_per_page' => 1)
            ); ?>
            <?php while( $catquery->have_posts() ) : $catquery->the_post(); ?>
                <?php the_post_thumbnail( 'post-thumb' ); ?>
                <div class="exposition-info">
                    <p><?php echo get_post_meta($post->ID, 'texto-bienvenida', true); ?></p>
                    <h3><?php the_title(); ?></h3>
                    <p><?php echo get_post_meta($post->ID, 'info-home', true); ?></p>
                    <a href="<?php the_permalink(); ?>" class="btn-more">Más información</a>
                </div>
            <?php endwhile; wp_reset_postdata(); ?>
        </div>
    </section>

    <section class="sec-activities">
        <div class="activities-container">
            <div class="snapshot">
                <div class="category instagram">Instagram</div>
                <?php if (dynamic_sidebar('instagram-widget')) : else : endif; ?>
            </div>
            <!--EMPIEZA EL LOOP-->
            <?php $query = new WP_Query( array(
                'cat' => '-20',
                'category_name' => 'conferencias,cursos,seminarios,talleres,visitas,exposiciones,activaciones',
                'posts_per_page' => 8 )
            ); ?>
            <?php while( $query->have_posts() ) : $query->the_post(); ?>
                <div class="snapshot">
                    <div class="category"><?php the_category(); ?></div>
                    <?php if (class_exists('MultiPostThumbnails')) : 
                        MultiPostThumbnails::the_post_thumbnail(get_post_type(), 'home-thumb');
                    endif; ?>
                    <h3><?php the_title(); ?></h3>
                    <a href="<?php the_permalink(); ?>" class="btn-more">Más información</a>
                </div>
            <?php endwhile; wp_reset_postdata(); ?>
            <!--TERMINA EL LOOP-->
        </div>
    </section>
</div>

<?php query_posts('category_name=Mensaje'); if (have_posts()) : while(have_posts()) : the_post();?>
    <div class="overlay-mensaje">
        <div class="actions-content">
            <a href="javascript:void(0);" class="btn-close">X</a>
        </div><!-- .actions-content -->
        <div class="container">
            <div class="popup-image-hor">
                <?php the_content(); ?>
            </div><!-- .popup-image-hor -->
        </div><!-- .container -->
    </div><!-- .overlay-mensaje -->
<?php endwhile; else: ?>
    <p style="display:none;"><?php _e('Lo sentimos, ningún post cumple con los criterios de búsqueda.'); ?></p>
<?php endif; ?>

<?php get_footer(); ?>