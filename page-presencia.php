<?php get_header(); ?>
<!-- Escupe la ruta hacia la carpeta raiz del templete <?php echo get_template_directory_uri(); ?> -->

<div class="container">
    <section class="section">
        <h2><span>Presencia</span></h2>
        <ul class="breadcrumb">
            <li><a href="javascript:void(0);">Inicio</a> / </li>
        </ul>
        <!--EMPIEZA EL LOOP-->
        <div class="content presence">
            <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
                <?php the_content(); ?>
            <?php endwhile; else: ?>
                <p><?php _e('Lo sentimos, ningún post cumple con los criterios de búsqueda.'); ?></p>
            <?php endif; ?>
        </div>
        <!--TERMINA EL LOOP-->
    </section>
</div>

<?php get_footer(); ?>