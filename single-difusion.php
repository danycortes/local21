<?php get_header(); ?>
<!-- Escupe la ruta hacia la carpeta raiz del templete <?php echo get_template_directory_uri(); ?> -->

<div class="container">
    <section class="section">
        <h2><span>Prensa</span></h2>
        <ul class="breadcrumb">
            <li><a href="javascript:void(0);">Inicio</a> / </li>
            <li><a href="javascript:void(0);">Prensa</a></li>
        </ul>
        <!--EMPIEZA EL LOOP-->
        <div class="content">
            <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
                <div class="info difusion">
                    <h3><?php the_title(); ?></h3>
                    <?php the_content(); ?>
                </div>
            <?php endwhile; else: ?>
                <p><?php _e('Lo sentimos, ningún post cumple con los criterios de búsqueda.'); ?></p>
            <?php endif; ?>
        </div>
        <!--TERMINA EL LOOP-->
    </section>
</div>

<?php get_footer(); ?>