$(document).ready(function(){
    /**
     * Navegación por medio de id's con animación
     */
    $('a[href*=\\#]:not([href=\\#])').click(function() {
        $('a').removeClass('selected');
        $(this).addClass('selected');
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') 
               || location.hostname == this.hostname) {

        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
          if (target.length) {
               $('html,body').animate({
               scrollTop: 200
               }, 1500);
               return false;
            }
        }
    });
      


    /**
     * Module: General
     * Show or hide navigation menu
     */
    $('.main-header .menu-toggle, .main-nav .btn-close').click(function(){
        if ( $('.main-nav').css('margin-right') == '0px' ){
           $('.main-nav').animate( {'margin-right': '-=395'} );
        } else {
           $('.main-nav').animate( {'margin-right': '+=395'} );
        }
    });


    /** Module: Single exposiciones
     * Appends gallery content into another div
    */
    var gallery = $('.wc-gallery').contents();
    $('.content .single-gallery').append(gallery);


    /**
     * Module: Generl
     * Removes href from category links on main menu
     */
    $('.dropdown > a').removeAttr('href');


    /** Module: General
     * Drops the sub menus down and changes the arrow icon
    */
    $('.dropdown > a').click( function() {
        let list = $(this).parent();
        list.find('ul.sub-menu').toggle();
        $(this).find('span').toggleClass('down-arrow');
    });


    /** Module: General
     * Adds an arrow icon to every option in the main nav with the 'dropdown' class
    */
    $('.dropdown > a').prepend( `
        <span class="right-arrow"></span>
    `);

    /** Module: General
     * Shows the popup message when webpage is loaded
     */
    $('.overlay-mensaje').fadeIn(1000);

    /** Module: General
     * Closes the popup message when close button is clicked
     */
    $('.overlay-mensaje .btn-close').click(function(){
        $('.overlay-mensaje').fadeOut();
    });

});